# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('article', '0006_auto_20160216_0305'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='article',
            options={'ordering': ['-date_time'], 'verbose_name_plural': 'article'},
        ),
        migrations.AlterField(
            model_name='article',
            name='category',
            field=models.ForeignKey(to='article.Category'),
        ),
    ]
