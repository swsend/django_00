# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.db import models, connection
from django.db.models import permalink

from django.utils import timezone
from django.contrib import admin
from django.core.urlresolvers import reverse

from django.template.defaultfilters import slugify
from tinymce.models import HTMLField

# Create your models here.

class Category(models.Model):
    name = models.CharField(max_length=50, unique=True)
    slug = models.SlugField()

    class Meta:
        verbose_name_plural='Category'
    
    def __unicode__(self):
        return self.name

class Article(models.Model):
    
    
    title = models.CharField(max_length = 100)
    slug = models.CharField('slug', max_length=255, blank=True)
    
    category = models.ForeignKey(Category)
    date_time = models.DateTimeField(auto_now_add = True)
    content =  models.TextField(blank = True, null = True) # blog article contetn

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ['-date_time']
        verbose_name_plural = "article"

    def get_absolute_url(self):
        
        return ('cms-article', (), {'slug': self.slug})

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Article, self).save(*args, **kwargs)

