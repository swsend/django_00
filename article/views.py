# -*- coding: utf-8 -*-

from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse

from article.models import Article, Category

from django.http import Http404
from django.template import TemplateDoesNotExist

from django.views import generic

# Create your views here.
    
class IndexView(generic.ListView):
    template_name = 'article_list.html'
    context_object_name = 'article_list'
    def get_queryset(self):
        #return  return Post.objects.filter(category__slug=self.kwargs['slug'])
        return Article.objects.all()




class DetailView(generic.DetailView):
    model = Article
    template_name = 'article_detail.html'
    def get_queryset(self):
        return Article.objects.all()

def category(request, slug):
    category = get_object_or_404(Category, Slug=slug)
    article_list = Article.objects.filter(category=category)
    heading = "Category： %s" % category.name
    return render("article_list.html")

        

